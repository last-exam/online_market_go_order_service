package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"last-exam/online_market_go_order_service/genproto/order_service"
	"last-exam/online_market_go_order_service/models"
	"last-exam/online_market_go_order_service/pkg/helper"
	"last-exam/online_market_go_order_service/storage"
)

type OrderRepo struct {
	db *pgxpool.Pool
}

func NewOrderRepo(db *pgxpool.Pool) storage.OrderRepoI {
	return &OrderRepo{
		db: db,
	}
}

func (c *OrderRepo) Create(ctx context.Context, req *order_service.CreateOrder) (resp *order_service.OrderPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "order" (
				id,
				user_id,
				product_id,
				order_amount,
				updated_at
			) VALUES ( $1, $2, $3, $4, now() )
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.UserId,
		req.ProductId,
		req.OrderAmount,
	)

	if err != nil {
		return nil, err
	}

	return &order_service.OrderPrimaryKey{Id: id.String()}, nil
}

func (c *OrderRepo) GetByPKey(ctx context.Context, req *order_service.OrderPrimaryKey) (resp *order_service.Order, err error) {

	fmt.Println(req)

	query := `
		SELECT
			id,
			user_id,
			product_id,
			order_amount,
			created_at,
			updated_at
		FROM "order"
		WHERE deleted_at IS NULL AND id = $1 AND user_id = $2
	`

	var (
		id           sql.NullString
		user_id      sql.NullString
		product_id   sql.NullString
		order_amount sql.NullInt32
		createdAt    sql.NullString
		updatedAt    sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id, req.UserId).Scan(
		&id,
		&user_id,
		&product_id,
		&order_amount,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &order_service.Order{
		Id:          id.String,
		UserId:      user_id.String,
		ProductId:   product_id.String,
		OrderAmount: order_amount.Int32,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *OrderRepo) GetAll(ctx context.Context, req *order_service.GetListOrderRequest) (resp *order_service.GetListOrderResponse, err error) {

	resp = &order_service.GetListOrderResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		// filter = " WHERE TRUE"
		sort = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			user_id,
			product_id,
			order_amount,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "order"
		WHERE deleted_at IS NULL
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += " AND user_id = '" + fmt.Sprint(req.UserId) + "'"

	query += sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id           sql.NullString
			user_id      sql.NullString
			product_id   sql.NullString
			order_amount sql.NullInt32
			createdAt    sql.NullString
			updatedAt    sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&user_id,
			&product_id,
			&order_amount,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Orders = append(resp.Orders, &order_service.Order{
			Id:          id.String,
			UserId:      user_id.String,
			ProductId:   product_id.String,
			OrderAmount: order_amount.Int32,
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}

	return
}

func (c *OrderRepo) Update(ctx context.Context, req *order_service.UpdateOrder) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "order"
			SET
				user_id = :user_id,
				product_id = :product_id,
				order_amount = :order_amount,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":           req.GetId(),
		"user_id":      req.GetUserId(),
		"product_id":   req.GetProductId(),
		"order_amount": req.GetOrderAmount(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *OrderRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"order"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *OrderRepo) Delete(ctx context.Context, req *order_service.OrderPrimaryKey) error {

	query := `UPDATE "order" SET deleted_at = now() WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
