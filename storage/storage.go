package storage

import (
	"context"

	"last-exam/online_market_go_order_service/genproto/order_service"
	"last-exam/online_market_go_order_service/models"
)

type StorageI interface {
	CloseDB()
	Order() OrderRepoI
}

type OrderRepoI interface {
	Create(ctx context.Context, req *order_service.CreateOrder) (resp *order_service.OrderPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *order_service.OrderPrimaryKey) (resp *order_service.Order, err error)
	GetAll(ctx context.Context, req *order_service.GetListOrderRequest) (resp *order_service.GetListOrderResponse, err error)
	Update(ctx context.Context, req *order_service.UpdateOrder) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *order_service.OrderPrimaryKey) error
}
