package client

import (
	"last-exam/online_market_go_order_service/config"

	"last-exam/online_market_go_order_service/genproto/product_service"
	"last-exam/online_market_go_order_service/genproto/user_service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	ProductService() product_service.ProductServiceClient
	UserService() user_service.UserServiceClient
}

type grpcClients struct {
	productService product_service.ProductServiceClient
	userService    user_service.UserServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {

	// Product Service...
	connProductService, err := grpc.Dial(
		cfg.ProductServiceHost+cfg.ProductGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	// User Service...
	connUserService, err := grpc.Dial(
		cfg.UserAuthServiceHost+cfg.UserAuthGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		productService: product_service.NewProductServiceClient(connProductService),
		userService:    user_service.NewUserServiceClient(connUserService),
	}, nil
}

func (g *grpcClients) ProductService() product_service.ProductServiceClient {
	return g.productService
}

func (g *grpcClients) UserService() user_service.UserServiceClient {
	return g.userService
}
