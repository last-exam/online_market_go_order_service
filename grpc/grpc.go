package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"last-exam/online_market_go_order_service/config"
	"last-exam/online_market_go_order_service/genproto/order_service"
	"last-exam/online_market_go_order_service/grpc/client"
	"last-exam/online_market_go_order_service/grpc/service"
	"last-exam/online_market_go_order_service/pkg/logger"
	"last-exam/online_market_go_order_service/storage"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	order_service.RegisterOrderServiceServer(grpcServer, service.NewOrderService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
