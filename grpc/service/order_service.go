package service

import (
	"context"
	"errors"
	"fmt"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"last-exam/online_market_go_order_service/config"
	"last-exam/online_market_go_order_service/genproto/order_service"
	"last-exam/online_market_go_order_service/genproto/product_service"
	"last-exam/online_market_go_order_service/genproto/user_service"
	"last-exam/online_market_go_order_service/grpc/client"
	"last-exam/online_market_go_order_service/models"
	"last-exam/online_market_go_order_service/pkg/logger"
	"last-exam/online_market_go_order_service/storage"
)

type OrderService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*order_service.UnimplementedOrderServiceServer
}

func NewOrderService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *OrderService {
	return &OrderService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *OrderService) Create(ctx context.Context, req *order_service.CreateOrder) (resp *order_service.Order, err error) {

	i.log.Info("---CreateOrder------>", logger.Any("req", req))

	product, err := i.services.ProductService().GetByID(ctx, &product_service.ProductPrimaryKey{Id: req.ProductId, MessageStatus: "order"})
	if err != nil {
		i.log.Error("!!!GetproductByID->productCategory->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if req.OrderAmount < 1 {
		i.log.Error("!!!GetproductByID->productCategory->Get---> Invalid order amount")
		return nil, status.Error(codes.InvalidArgument, errors.New("Invalid order amount").Error())
	}

	if req.OrderAmount > product.ProductAmount || product.ProductAmount < 1 {
		i.log.Error("!!!GetproductByID->productCategory->Get---> Invalid order amount")
		return nil, status.Error(codes.InvalidArgument, errors.New("Invalid order amount").Error())
	}

	if req.UserId == product.OwnerId {
		i.log.Error("!!!CreateOrder->Order->Create---> Seller and Buyer ID are same!")
		return nil, status.Error(codes.InvalidArgument, errors.New("Seller and Buyer ID are same!").Error())
	}

	_, err = i.services.UserService().UserMoneyExchangeWithdraw(ctx, &user_service.UserTransaction{UserId: req.UserId, AmountOfMoney: float64(req.OrderAmount) * product.Price})
	if err != nil {
		i.log.Error("!!!TransactionWithdrawal->user->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	_, err = i.services.ProductService().ProductMoneyExchange(ctx, &product_service.ProductTransaction{ProductId: req.ProductId, AmountOfMoney: float64(req.OrderAmount) * product.Price, OrderedAmount: req.OrderAmount})
	if err != nil {
		i.log.Error("!!!ProductMoneyExchange->productCategory->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	pKey, err := i.strg.Order().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateOrder->Order->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	fmt.Println("::", pKey)

	pKey.UserId = req.UserId

	resp, err = i.strg.Order().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyOrder->Order->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *OrderService) GetByID(ctx context.Context, req *order_service.OrderPrimaryKey) (resp *order_service.Order, err error) {

	i.log.Info("---GetOrderByID------>", logger.Any("req", req))

	resp, err = i.strg.Order().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetOrderByID->Order->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	product, err := i.services.ProductService().GetByID(ctx, &product_service.ProductPrimaryKey{Id: resp.ProductId, MessageStatus: "order"})
	if err != nil {
		i.log.Error("!!!GetproductByID->productCategory->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	ex := &order_service.OrderCategoryLists{
		Id:       product.ProductCategories.Id,
		Name:     product.ProductCategories.Name,
		ParentId: product.ProductCategories.ParentId,
	}

	resp.OrderedProduct = &order_service.OrderedProduct{
		ProductId:         product.Id,
		ProductName:       product.Name,
		ProductStatus:     product.Status,
		ProductPrice:      product.Price,
		ProductCategories: ex,
		CreatedAt:         product.CreatedAt,
		UpdatedAt:         product.UpdatedAt,
	}

	resp.OrderedProduct.Owner = &order_service.OrderProductOwner{
		OwnerId:    product.Owner.OwnerId,
		OwnerName:  product.Owner.OwnerName,
		OwnerPhone: product.Owner.OwnerPhone,
		CreatedAt:  product.Owner.CreatedAt,
		UpdatedAt:  product.Owner.UpdatedAt,
	}

	return
}

func (i *OrderService) GetList(ctx context.Context, req *order_service.GetListOrderRequest) (resp *order_service.GetListOrderResponse, err error) {

	i.log.Info("---GetOrders------>", logger.Any("req", req))

	resp, err = i.strg.Order().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetOrders->Order->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	for k := 0; k <= int(resp.Count); k++ {
		product, err := i.services.ProductService().GetByID(ctx, &product_service.ProductPrimaryKey{Id: resp.Orders[k].ProductId, MessageStatus: "order"})
		if err != nil {
			i.log.Error("!!!GetproductByID->productCategory->Get--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		resp.Orders[k].OrderedProduct = &order_service.OrderedProduct{
			ProductId:     product.Id,
			ProductName:   product.Name,
			ProductStatus: product.Status,
			ProductPrice:  product.Price,
			CreatedAt:     product.CreatedAt,
			UpdatedAt:     product.UpdatedAt,
		}

		resp.Orders[k].OrderedProduct.ProductCategories = &order_service.OrderCategoryLists{
			Id:       product.ProductCategories.Id,
			Name:     product.ProductCategories.Name,
			ParentId: product.ProductCategories.ParentId,
		}

		resp.Orders[k].OrderedProduct.Owner = &order_service.OrderProductOwner{
			OwnerId:    product.Owner.OwnerId,
			OwnerName:  product.Owner.OwnerName,
			OwnerPhone: product.Owner.OwnerPhone,
			CreatedAt:  product.Owner.CreatedAt,
			UpdatedAt:  product.Owner.UpdatedAt,
		}
	}

	return
}

func (i *OrderService) Update(ctx context.Context, req *order_service.UpdateOrder) (resp *order_service.Order, err error) {

	i.log.Info("---UpdateOrder------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Order().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateOrder--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Order().GetByPKey(ctx, &order_service.OrderPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetOrder->Order->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *OrderService) UpdatePatch(ctx context.Context, req *order_service.UpdatePatchOrder) (resp *order_service.Order, err error) {

	i.log.Info("---UpdatePatchOrder------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Order().UpdatePatch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchOrder--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Order().GetByPKey(ctx, &order_service.OrderPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetOrder->Order->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *OrderService) Delete(ctx context.Context, req *order_service.OrderPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteOrder------>", logger.Any("req", req))

	err = i.strg.Order().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteOrder->Order->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
