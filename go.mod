module last-exam/online_market_go_order_service

go 1.18

require (
	github.com/google/uuid v1.1.2
	github.com/jackc/pgx/v4 v4.17.1
	github.com/joho/godotenv v1.4.0
	github.com/spf13/cast v1.5.0
	github.com/streamingfast/logging v0.0.0-20220813175024-b4fbb0e893df
	go.uber.org/zap v1.23.0
	golang.org/x/crypto v0.0.0-20220826181053-bd7e27e6170d
	google.golang.org/grpc v1.49.0
)

require (
	github.com/golang/protobuf v1.5.2
	github.com/pkg/errors v0.9.1 // indirect
	google.golang.org/protobuf v1.28.1
)

require (
	github.com/blendle/zapdriver v1.3.1 // indirect
	github.com/jackc/chunkreader/v2 v2.0.1 // indirect
	github.com/jackc/pgconn v1.13.0 // indirect
	github.com/jackc/pgio v1.0.0 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgproto3/v2 v2.3.1 // indirect
	github.com/jackc/pgservicefile v0.0.0-20200714003250-2b9c44734f2b // indirect
	github.com/jackc/pgtype v1.12.0 // indirect
	github.com/jackc/puddle v1.3.0 // indirect
	github.com/logrusorgru/aurora v2.0.3+incompatible // indirect
	github.com/mitchellh/go-testing-interface v1.14.1 // indirect
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.7.0 // indirect
	golang.org/x/net v0.0.0-20220722155237-a158d28d115b // indirect
	golang.org/x/sys v0.0.0-20220722155257-8c9f86f7a55f // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013 // indirect
)
